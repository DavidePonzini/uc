import React, { Component } from 'react'
import { View, StyleSheet, TouchableNativeFeedback, Text } from 'react-native'

export default class ToggleButton extends React.Component {

  	render() {
		return (
	  		<TouchableNativeFeedback 
	  			onPress = {this.props.onPress}>
	  			<View style = {this.props.selected() ? {backgroundColor: '#e0e0e0'} : {}}>
					<Text style = {this.props.textStyle}>{this.props.label}</Text>
				</View>
	  		</TouchableNativeFeedback>
		);
  	}
}