import React, { Component } from 'react'
import { Platform, StyleSheet, View, Text, TouchableOpacity, Alert, Dimensions } from 'react-native'
import { Table, TableWrapper, Row, Cell } from 'react-native-table-component'
   
export default class Timetable extends Component {

    constructor(props) {
        super(props);
        this.state = {
        tableHead: ['Device', 'Time', ''],
        tableData: [
            ['1', '2', '3'],
            ['a', 'b', 'c'],
            ['1', '2', '3'],
            ['a', 'b', 'c']
        ]
        }
    }
 
  alertIndex(index) {
    Alert.alert(`This is row ${index + 1}`);
  }
 
    render() {
        var {height, width} = Dimensions.get('window')
        const state = this.state;
        const element = (data, index) => (
            <TouchableOpacity onPress={() => this._alertIndex(index)}>
            <View style={styles.btn}>
                <Text style={styles.btnText}>button</Text>
            </View>
            </TouchableOpacity>
        );
     
        return (
            <Table width = {width} borderStyle={{borderColor: 'transparent'}}>
                  <Row data={state.tableHead} style={styles.head} textStyle={styles.text}/>
                {
                    state.tableData.map((rowData, index) => (
                    <TableWrapper key={index} style={styles.row}>
                        {rowData.map((cellData, cellIndex) => (
                            <Cell key={cellIndex} data={cellIndex === 3 ? element(cellData, index) : cellData} textStyle={styles.text}/>
                        ))
                        }
                    </TableWrapper>
                    ))
                }
            </Table>
        )
    }
}

       
const styles = StyleSheet.create({
    header: {
        flex: 1,
        paddingTop: 25,
        fontSize: 22,
        paddingLeft: 10,
        textAlign: 'center',
        backgroundColor: '#0baf00',
    },

    head: { height: 40, backgroundColor: '#808B97' },
    text: { margin: 6, fontSize: 20, textAlign: 'center' },
    row: { flexDirection: 'row', backgroundColor: '#FFF1C1' },
    btn: { width: 58, height: 18, backgroundColor: '#78B7BB',  borderRadius: 2 },
    btnText: { textAlign: 'center', color: '#fff' }
});