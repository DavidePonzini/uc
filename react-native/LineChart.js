import React, { Component } from 'react'
import { SectionList, StyleSheet, View, Text, TouchableNativeFeedback, Dimensions, Button } from 'react-native'
import { createStackNavigator } from 'react-navigation';
import LineChart from 'react-native-responsive-linechart'


export default class Chart extends Component {
	
	state = {
		url: this.props.navigation.getParam('url').url+'?token=A1E-4U3AJ0LkPogUmvhDCPYQHM2F0TUFvQ',
		data: [1,2],
		labels: [1,2]
	}

	componentDidMount = () => {
		this.fetchData()
	}

	fetchData() {
		fetch(this.state.url, {
			method: 'GET'
	  	})
	  	.then((response) => response.json())
	  	.then((responseJson) => {
	 
			// Parsing the json file in order to extract information
			console.log(responseJson)
			var results = responseJson.results
			var values = []
			var labels = []
			var num_elem = Math.min(10, results.length)

			for (var i=0; i < num_elem; i++) {
				labels.unshift(results[i].timestamp)
				values.unshift(results[i].value)
			}

			this.setState({
				data: values,
				labels: labels
			})
//			this.fetchData()
		})
		.catch((error) => {
			console.error(error);
		});
	}

	render() {
		var {height, width} = Dimensions.get('window')
		return (
			<View width={width} height={height-100} style={{marginTop: 30}}>
				<LineChart
					style={{ flex: 1 }}
					xLabels={this.state.labels}
					config={chart_config}
					data={this.state.data} />
			</View>
		)
	}
}

const chart_config = {
	line: {
		strokeWidth: 3,
		strokeColor: "#216D99"
	},
	area: {
		gradientFrom: "#ff3535", // top value
		gradientFromOpacity: 1,
		gradientTo: "#ffc934",  // bottom value
		gradientToOpacity: 0.4
	},
	yAxis: {
		labelColor: "#000",
		labelFormatter: v => Math.floor(v) + " W"
	},
	grid: {
		strokeColor: "#c8d6e5",
		
	},
	insetY: 10,
	insetX: 10,
	interpolation: "linear",
	backgroundColor: "#fff"
};