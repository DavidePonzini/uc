import DevicesPane from './DevicesPane'
import AmbientPane from './AmbientPane';
import { View, Text } from 'react-native'
import React, { Component } from 'react'
import BottomNavigation, { FullTab } from 'react-native-material-bottom-navigation'
import Icon from '@expo/vector-icons/MaterialCommunityIcons'
import TemperaturePane from './TemperaturePane'
import HumidityPane from './HumidityPane'

export default class App extends React.Component {
  tabs = [
    {
      key: 'temperature',
      icon: 'temperature-celsius',
      label: 'Temperature',
      barColor: '#ff9900',
      pressColor: 'rgba(255, 255, 255, 0.16)'
    },
    {
      key: 'humidity',
      icon: 'water',
      label: 'Humidity',
      barColor: '#2d8fff',
      pressColor: 'rgba(255, 255, 255, 0.16)'
    },
    {
      key: 'devices',
      icon: 'washing-machine',
      label: 'Devices',
      barColor: '#0baf00',
      pressColor: 'rgba(255, 255, 255, 0.16)'
    }
  ]

  state = {
    activeTab: this.tabs[0].key
  }

  renderIcon = icon => ({ isActive }) => (
    <Icon size={24} color="white" name={icon} />
  )

  renderTab = ({ tab, isActive }) => (
    <FullTab
      isActive={isActive}
      key={tab.key}
      label={tab.label}
      renderIcon={this.renderIcon(tab.icon)}
    />
  )

  
  render() {

  	var comp
  	switch(this.state.activeTab) 
  	{
  		case 'temperature':
  			comp = <TemperaturePane/>
  			break
  		case 'humidity':
  			comp = <HumidityPane/>
  			break
  		case 'devices': console.log(this.props.navigation.navigate)
  			comp = <DevicesPane navigation = {this.props.navigation}/>
  	}
  	
    return (
      <View style={{ flex: 1 }}>
        <View style={{ flex: 1 }}>
        {comp}
        </View>
        <BottomNavigation
          onTabPress={newTab => this.setState({ activeTab: newTab.key })}
          renderTab={this.renderTab}
          tabs={this.tabs}
        />
      </View>
    )
  }
}