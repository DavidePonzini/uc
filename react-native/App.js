import React, { Component } from 'react'
import { SectionList, StyleSheet, View, Text } from 'react-native'
import { createStackNavigator, } from 'react-navigation';
import BottomNav from './BottomNav'
import Chart from './LineChart'

const RootStack = createStackNavigator(
{
	Main: BottomNav,
	Chart: Chart,
},
{
	initialRouteName: 'Main',
});

export default class App extends React.Component {
	render() {
		return <RootStack />;
	}
}

