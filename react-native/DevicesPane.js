import React, { Component } from 'react'
import { SectionList, StyleSheet, View, Text, TouchableNativeFeedback, Dimensions, Button } from 'react-native'
import ToggleButton from './ToggleButton'
import Timetable from './Timetable'
import DateTimePicker from 'react-native-modal-datetime-picker'
import Toast from 'react-native-simple-toast'

export default class DevicesPane extends Component {
   
	state = {
		data: '',
		buttonSelected: -1,
		showActions: false,
		showTimePicker: false,
		action: '',
		device: '',
	}

   	// Retrieving devices from Ubidots
   	componentDidMount = () => {
		fetch('http://things.ubidots.com/api/v1.6/datasources/5b8e5552591636086fab6a89/variables/?token=A1E-4U3AJ0LkPogUmvhDCPYQHM2F0TUFvQ', {
			method: 'GET'
	  	})
	  	.then((response) => response.json())
	  	.then((responseJson) => {
	 
			// Parsing the json file in order to extract information
			var variables = responseJson.results
			var ids = []
			for (var i=0; i < variables.length; i++)
			ids.push(variables[i].label)

			this.setState({
				data: ids
			})
			})
		.catch((error) => {
			console.error(error);
		});
	}


	onButtonPress(arg) {
  		this.setState({showActions: true, 
  			buttonSelected: arg,
  			device: arg})
	}


	onActionSelected(action) {
		if (action == 'timer') {
			this.setState({showTimePicker: true,
				action: action})
		}
		else 
			console.log(action)
 	}

 	timeSelected(time) {
 		this.setState({showTimePicker: false})
 		url = 'http://130.251.215.9:1880/action/device'
 		fetch(url, {
 			method: 'POST',
  			headers: {
    			Accept: 'application/json',
    			'Content-Type': 'application/json',
  			},
  			body: JSON.stringify({
    			time: time,
    			device: this.state.device,
    			action: 'set',
  			}),
		})
		.then((response) => response.json())
		.then((responseJson) => {
			
			Toast.show(responseJson.msg, Toast.LONG);
			console.log(responseJson)
		})
		.catch((error) => {
			console.error(error);
		});
		
 	}

 	hideTimePicker() {
 		this.setState({showTimePicker: false})
 	}

 	viewPlanning() {
 		console.log('Planning')
 	}


	formatText(text) {
		return text.replace(/^\w/, (chr) => chr.toUpperCase())
	}


   // Showing the result
   	render() {
   		var {height, width} = Dimensions.get('window')
        
		return (
	  		
		<View style = {{marginTop: 30}}>
			<Button onPress = {() => this.viewPlanning()}
      			title = 'View Planning'></Button>
			<SectionList style = {{marginTop: 10}}
				renderItem = {({item, index, section}) => (
				<ToggleButton onPress = {() => this.onButtonPress(item)} 
					selected = {() => this.state.buttonSelected == item}
					label = {this.formatText(item)}
					textStyle = {styles.item}>
				</ToggleButton>
				)}

				renderSectionHeader = {({section: {title}}) => (
		  			<Text style = {styles.header}>{title}</Text>
				)}

				sections={[
					{title: 'Devices', data: this.state.data},
		  		]}
		  
		  		keyExtractor = {(item, index) => item + index}
	  		/>
	  		
	  		<SectionList
				renderItem = {({item, index, section}) => (
					this.state.showActions &&
					<TouchableNativeFeedback
					 	key = {index}
					 	onPress = {() => this.onActionSelected(item.action)}
					 	background = {TouchableNativeFeedback.Ripple('#969696')}> 
					 	<View>
					 	<Text style = {styles.item}>{item.label}</Text>
					 	</View>
					</TouchableNativeFeedback>
				)}

				renderSectionHeader = {({section: {title}}) => (
					this.state.showActions &&
					<Text style = {styles.header}>{title}</Text>
				)}

				sections={[
					{title: 'Actions', data: [{label:'Set time', action:'timer'},
												{label:'View consumption', action:'consumption'}]}
				]}
				keyExtractor = {(item, index) => item + index}
			/>
			
			<View>
				<DateTimePicker
					isVisible={this.state.showTimePicker}
          			onConfirm={(time) => this.timeSelected(time)}
          			onCancel={() => this.hideTimePicker()}
          			mode='time'
        		/>
      		</View>
			<Timetable style = {[styles.container, {width: {width}}, {bottom: 20}]}></Timetable>
			
		</View>
	  	)
	}
}


// Styles 
const styles = StyleSheet.create({
  header: {
		flex: 1,
		paddingTop: 25,
		fontSize: 22,
		paddingLeft: 10,
		textAlign: 'center',
		backgroundColor: '#0baf00',
	},

	item: {
		padding: 10,
		height: 44,
		fontSize: 20,
	},

	button: {
		alignItems: 'center',
		backgroundColor: '#ffffff',
		padding: 10,
		color: '#000',
  },
    
    container: { 
      	flex: 1, 
      	padding: 30, 
      	backgroundColor: '#fff',
    },
})
