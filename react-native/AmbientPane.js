import React, { Component } from 'react'
import { SectionList, StyleSheet, View, Text, TouchableNativeFeedback, Alert, TextInput, Button } from 'react-native'
import ToggleButton from './ToggleButton'
import Toast from 'react-native-simple-toast'
//import Global from './Global'

const server_ip = 'http://130.251.248.141:1880'

export default class AmbientPane extends Component {

	state = {
		data: '',
		avg: '',
		showActions: false,
		showInput: false,
		buttonSelected: -1,
		action: '',
		text: '',
	}

	componentDidMount = () => {
		var url
			switch(this.props.tab) {
				case 'temperature':
					url = 'http://things.ubidots.com/api/v1.6/datasources/5b9a1c71c03f9744b05fc6cf/variables/?token=A1E-4U3AJ0LkPogUmvhDCPYQHM2F0TUFvQ'
					break
				case 'humidity':
					url = 'http://things.ubidots.com/api/v1.6/datasources/5b9a272c5916363117723b61/variables/?token=A1E-4U3AJ0LkPogUmvhDCPYQHM2F0TUFvQ'
					break
			}

			fetch(url, {method: 'GET'})
			.then((response) => response.json())
			.then((responseJson) => {

				// Parsing the json file in order to extract information
				var rooms = responseJson.results
				var avg = 0
				 
				for (var i=0; i<rooms.length; i++)
					avg += rooms[i]['last_value']['value']

				avg = avg/rooms.length

				this.setState({
					data: rooms,
					avg: avg
				})
			})
			.catch((error) => {
				console.error(error);
			});
	 }

	get_header_color = (arg) => {
		switch (arg) {
			case 'temperature':
				return '#ff9900'
			case 'humidity':
				return '#2d8fff'
		}
	}


  	onButtonPress(arg) {
  		this.setState({showActions: true, 
  			buttonSelected: arg})
	}
 
 	
	onActionSelected(action) {
		if (action == 'turnoff') {
			this.setState({showInput: false,
				action: action})
			Alert.alert(
				this.formatText(this.state.buttonSelected),
				'Are you sure you want to turn this off?',
				[{text: 'Cancel', style: 'cancel'},
				{text: 'OK', onPress: () => this.sendAction('turnoff')},],
				{cancelable: true})
		}
		else 
			this.setState({showInput: true, action: action})
 	}


 	sendAction() {

 		url = server_ip+'/action/ambient'
 		fetch(url, {
 			method: 'POST',
  			headers: {
    			Accept: 'application/json',
    			'Content-Type': 'application/json',
  			},
  			body: JSON.stringify({
    			room: this.state.buttonSelected,
    			action: this.state.action,
    			variable: this.props.tab,
    			target: this.state.text,
  			}),
		})
		.then((response) => response.json())
		.then((responseJson) => {
			Toast.show(responseJson.msg, Toast.LONG);
			console.log(responseJson)
		})
		.catch((error) => {
			console.error(error);
		});
	}


	formatText(text) {
		return text.replace(/^\w/, (chr) => chr.toUpperCase()).replace(/_/g, ' ')
	}


	render() {
			return (
				<View style = {{marginTop: 30}}>
					<Text style = {styles.item}>{this.formatText(this.props.tab)} : {this.state.avg}</Text>
					<SectionList
						renderItem = {({item, index, section}) => (
							<ToggleButton onPress = {() => this.onButtonPress(item.label)} 
								selected = {() => this.state.buttonSelected == item.label}
								label = {this.formatText(item.label) +' : '+ item.last_value.value}
								textStyle = {styles.item}>
							</ToggleButton>
						)}

						renderSectionHeader = {({section: {title}}) => (
							<Text style = {[styles.header,{backgroundColor: this.get_header_color(this.props.tab)}]}>{title}</Text>
						)}

						sections={[
							{title: 'Rooms', data: this.state.data}
						]}          
						keyExtractor = {(item, index) => item + index}
					/>
					<SectionList
						renderItem = {({item, index, section}) => (
							this.state.showActions &&
							<TouchableNativeFeedback
							 	key = {index}
							 	onPress = {() => this.onActionSelected(item.action)}
							 	background = {TouchableNativeFeedback.Ripple('#969696')}> 
							 	<View>
							 	<Text style = {styles.item}>{item.label}</Text>
							 	</View>
							</TouchableNativeFeedback>
						)}

						renderSectionHeader = {({section: {title}}) => (
							this.state.showActions &&
							<Text style= {[styles.header,{backgroundColor: this.get_header_color(this.props.tab)}]}>{title}</Text>
						)}

						sections={[
							{title: 'Actions', data: [{label:'Increase/Decrease', action:'set'},
														{label:'Turn Off', action:'turnoff'}]}
						]}
						keyExtractor = {(item, index) => item + index}
					/>
					<SectionList
						renderItem = {({item, index, section}) => (
							this.state.showInput &&
							<View>
							<TextInput
        						style={{paddingLeft: 10, height: 100, borderColor: 'gray', borderWidth: 1}}
        						onChangeText={(text) => this.setState({text})}
        						value={this.state.text}
        						placeholder = 'Insert temperature here'
        						keyboardType = 'numeric'
        						maxLength={2}>
      						</TextInput>
      						<Button style = {styles.button} onPress = {() => this.sendAction()}
      						title = 'OK'></Button>
      						</View>
						)}

						renderSectionHeader = {({section: {title}}) => (
							this.state.showInput &&
							<Text style= {[styles.header,{backgroundColor: this.get_header_color(this.props.tab)}]}>{title}</Text>
						)}

						sections={[
							{title: 'Increase/Decrease', data: ['']}
						]}
					
						keyExtractor = {(item, index) => item + index}
					/>
				</View>
			)
	 }
}

// Styles 
const styles = StyleSheet.create({
	header: {
	 	flex: 1,
	 	paddingTop: 25,
	 	fontSize: 22,
	 	paddingLeft: 10,
	 	textAlign: 'center',
	},

	item: {
		padding: 10,
		height: 44,
		fontSize: 20,
	},

	button: {
    	alignItems: 'center',
    	backgroundColor: '#ffffff',
    	padding: 10,
    	color: '#000',
  },
})